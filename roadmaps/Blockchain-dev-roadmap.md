# Blockchain developer Roadmap 

* [YT-link](https://www.youtube.com/watch?v=SRyfhHppQik)


## Types of positions <a name="positions"></a>

* Core blockchain 
* Blockchain software 

### Core blockchain <a name="core"></a>

* Architecutre 
* protocols

### Blockchain software <a name="software"></a>

* build app 
  * smart contract
* everything in the sistem work 

## Things to learn <a name="things-learn"></a>

* Programming 
* Data structures 
* Cryptography 

* ZTM
* Coursera
* Courses 

## Skillset <a name="skillset"></a>

* Programming 
  * C++ 
  * Python
  * Solidity
* Cryptography 
  * Hash functions 
  * Digital Signatures
  * Encryption algorithms 
* Distributed systems 
  * Proof of work 
  * Proof of state 

## Step by step <a name="step-by-step"></a>

1. Build a strong foundation in programming 
  * Learn ( some of these: java, python, solidity)
  * Hands on simple projects or scripts 
2. Learn about blockchain fundamentals 
  * Desentralize network work 
  * Proof of work 
  * proof of state
3. Get familiar with cryptography
  * Hashing 
  * Public & private keys
  * Digital Signatures 
4. Learn about smart contracts 
  * write them / build them 
5. Work with blockchain platforms 
6. Contribute to open source proyects 
7. Build your own blockchain proyects 
8. Keep learning and staying update 


