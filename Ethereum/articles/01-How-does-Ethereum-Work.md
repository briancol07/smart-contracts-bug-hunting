# How does Ethereum work? 

* [Article](https://preethikasireddy.medium.com/how-does-ethereum-work-anyway-22d1df506369)

In essence a public database that keeps a permanent record of digital transactions.
Does not require any central authority to maintain and secure it. Operates as a "trustless" transactional system.

> Use blockchain Paradigm 

## Blockchain definitions

> A blockchain is a "cryptographically secure transactional singleton machine with shared-state"


### Cryptographically secure: 

Means that the creation of digital currency is secured by complex mathematical algorithms that are obscenely hard to break.

### Transactional singleton machine 

Means that there's a single canonical instance of the machine responsible for all the transactions being created in the system 

### Shared state 

Means that the state stored on this machine is shared nad open to everyone 

## Blockchain Paradigm

A transaction-based state machine : refers to something that will read a series of input and base on those inputs will transition to a new state 

![reading](../img/reading.webp)

In Ethereum it begins with a "genesis state", analog to a blank slate, before any transaction have happened on the network. When a transaction is executed, this genesis state transitions into some final state. 

![State](../img/state.png)

Each state has  millions of transactions. These transactions are grouped into "blocks". A block contain a series of transactions, and each block is chained together with its previous block 

![block](../img/block.png)

Each transition from one state to another must be valid, to be considered as valid it must go through a validation process known as mining. When a group of nodes expend their compute resources to create a block of valid transactions 
  
Each time a miner provides mathematical "proof" when suminiting a block to the blockchain, and this proof acts as guarantee: if the proof exists,the block must be valid 

A miner who validates a new block is rewarded with a certain amount of valuer for doing this work . In this case a digital token called "Ether" 

Whenever multiple paths are generated, a "fork" occurs. We typically want to avoid forks, because they disrupt the system and force people to choose which chain they "believe" 

![fork](../img/fork.png)

To determine which path is most valid and prevent multiple chains , ethereum uses a mechanism called **Ghost protocol** : "Greedy heaviest observed subtree"

Pick the path that has had the most computation done upon it 

![ghost](../img/ghost.png)

> Ethereum uses hash function [keccak-256](https://ethereum.stackexchange.com/questions/550/which-cryptographic-hash-function-does-ethereum-use) 

## Accounts 

The shared state of ethereum is comprised of many small objects ("accounts") that are able to interact with one another through a message passign framework . Each account has a state associated with it and a 20-byte address . An address in ethereum is a 160-bit identifies that is used to identify any account

* Two types 
  * Externally owned : controlled by private keys and have no code associated with them 
  * Contract accounts: Which are controlled by their contract code and have code associated with them 

### Externally account 

Can send message to other externally owned account or to other contract accounts by creating and signing a transaction using its private key 

* A message between two externally owned account is a value transfer 
* A message  from an externally account to a contract account activate the contract account's code allowing to perform actions 
  * Transfer tokens 
  * Write to internal storage 
  * Mint new tokens 
  * Perform calculations 
  * Create contracts 

### Contract account 

Can't initiate a new transaction on their own.  They can only fire transactions in response to other transactions they have received 

![accounts](../img/accounts.png)

## Account State
