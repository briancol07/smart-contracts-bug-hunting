# Smart-Contracts-Bug-Hunting

This is the beginning of my journey with bugbouny Smart Contracts

## Usefull links

* [Hacken Proof](https://hackenproof.com/blog/industry-news/how-to-smart-contracts-bug-hunting)
* [Medium](https://medium.com/@Consensys/a-101-noob-intro-to-programming-smart-contracts-on-ethereum-695d15c1dab4)
* [Solidty Lang](https://docs.soliditylang.org/en/latest/introduction-to-smart-contracts.html)
* [Ethereum Org](https://ethereum.org/en/)

## Stake 

* [AAVE](https://aave.com/help/safety-module/stake)

## Roadmap

* [Roadmap](https://roadmap.sh/blockchain)
* YT
  * [Daniel-Tech-data](https://www.youtube.com/watch?v=SRyfhHppQik)
  * [Patrick-Collins](https://www.youtube.com/watch?v=umepbfKp5rI)
    * [Smart Contract-Defi](https://www.youtube.com/watch?v=pUWmJ86X_do)
  * [Solidity-Naz-Dumaskyy](https://www.youtube.com/watch?v=AYpftDFiIgk)

### ChatGPT plan 

* [ChatGPT](./Chat-gpt-plan.md)

## Articles 

* [Post-Exploit options](https://medium.com/linear-finance/post-exploit-options-and-opportunities-d5540e04a055)


## Courses 

* [edx](https://www.edx.org/learn/blockchain/the-linux-foundation-blockchain-understanding-its-uses-and-implications) 
